import pytest

from whichpkg.__main__ import main, parse_html, PackageInfo, consume_pkg_info

from dirs import DATA_DIR
from myvcr import myvcr


@myvcr.use_cassette
def test_main(capsys):
    main(["pg_config"])
    captured = capsys.readouterr()
    assert "pg_config" in captured.out


def test_parse_html_no_arch():
    html = (DATA_DIR / "pg_config.html").read_text()
    packages = set(x.name for x in parse_html(html))

    assert packages == {"libpq-dev", "postgresql-common", "postgresql-client-14"}


def test_parse_html_arch():
    html = (DATA_DIR / "mplayer.html").read_text()
    packages = set(parse_html(html))

    assert packages == {
        PackageInfo(
            "/usr/share/bash-completion/completions/mplayer",
            "bash-completion",
            "",
        ),
        PackageInfo(
            "/usr/lib/hollywood/mplayer",
            "hollywood",
            "",
        ),
        PackageInfo(
            "/usr/bin/mplayer",
            "mplayer",
            "not i386",
        ),
        PackageInfo(
            "/usr/lib/mime/packages/mplayer",
            "mplayer",
            "not i386",
        ),
        PackageInfo(
            "/usr/share/lintian/overrides/mplayer",
            "mplayer",
            "not i386",
        ),
        PackageInfo(
            "/usr/share/vdr-plugin-mplayer/mplayer",
            "vdr-plugin-mplayer",
            "not i386",
        ),
    }


def test_parse_html_multi_arch():
    html = (DATA_DIR / "gcc.html").read_text()
    dct = {x.path: x.arch for x in parse_html(html)}

    assert dct["/usr/lib/nvidia-cuda-toolkit/bin/gcc"] == "amd64, ppc64el, arm64"


@pytest.mark.parametrize(
    "text,expected",
    (
        ("foo", [("foo", "")]),
        ("foo, bar", [("foo", ""), ("bar", "")]),
        ("foo [x86, x87]", [("foo", "x86, x87")]),
        (
            "foo [x86, x87], bar [x88, x89]",
            [
                ("foo", "x86, x87"),
                ("bar", "x88, x89"),
            ],
        ),
    ),
)
def test_consume_pkg_info(text, expected):
    result = list(consume_pkg_info(text))
    assert result == expected
