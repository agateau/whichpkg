from pathlib import Path


TEST_DIR = Path(__file__).parent

DATA_DIR = TEST_DIR / "data"

CASSETTES_DIR = TEST_DIR / "cassettes"
