import vcr

from dirs import CASSETTES_DIR


myvcr = vcr.VCR(
    cassette_library_dir=str(CASSETTES_DIR),
    path_transformer=vcr.VCR.ensure_suffix(".yaml"),
    decode_compressed_response=True,
    ignore_localhost=True,
    match_on=["method", "url"],
    serializer="yaml",
    record_mode="once",
)
