"""
Scrape packages.ubuntu.com to list packages containing a given file.
"""

import argparse
import re
import subprocess
import sys

from dataclasses import dataclass
from typing import Iterable, NamedTuple

import urllib.error
import urllib.parse
import urllib.request

from bs4 import BeautifulSoup
from tabulate import tabulate


PKG_URL = "https://packages.ubuntu.com/search?lang=en&suite=%(suite)s&arch=any&mode=exactfilename&searchon=contents&keywords=%(keywords)s"


class PackageInfo(NamedTuple):
    path: str
    name: str
    arch: str


def consume_pkg_info(text) -> Iterable[tuple[str, str]]:
    # text can be something like this:
    #
    # foo [arch1, arch2], bar [arch1, arch3]
    while True:
        text = text.strip()
        match = re.match(r"(?P<name>[^, ]*)( \[(?P<arch>.+?)\])?", text)
        if not match:
            print(f"Failed to extract name and arch from '{text}'")
            return

        name = match.group("name")
        arch = match.group("arch") or ""
        yield (name, arch)

        text = text[match.end() + 2 :]
        if not text:
            return


def parse_html(html) -> Iterable[PackageInfo]:
    try:
        soup = BeautifulSoup(html, features="html.parser")
    except Exception as exc:
        print("*" * 40)
        print("Failed to parse this HTML:")
        print(html)
        print("*" * 40)
        print(exc)
        sys.exit(1)

    for tr in soup.findAll("tr"):
        td_list = tr.findAll("td")
        if not td_list:
            continue
        path_td, pkg_td = td_list
        path = path_td.text

        for name, arch in consume_pkg_info(pkg_td.text):
            yield PackageInfo(path, name, arch)


def find_current_distro():
    result = subprocess.run(["lsb_release", "-c", "-s"], capture_output=True, text=True)
    return result.stdout.strip()


def fetch_page(distro: str, filename: str) -> str:
    url = PKG_URL % dict(suite=distro, keywords=urllib.parse.quote(filename))
    try:
        with urllib.request.urlopen(url) as fl:
            return fl.read().decode("utf-8")
    except urllib.error.URLError as exc:
        sys.exit(f"Network error: {exc}")


def main(argv: list[str] | None = None):
    if argv is None:
        argv = sys.argv[1:]

    parser = argparse.ArgumentParser()
    parser.description = __doc__

    parser.add_argument(
        "-d",
        "--distro",
        metavar="DISTRO",
        help="Search for packages in DISTRO instead of the default one",
    )

    parser.add_argument(
        "--debug", action="store_true", help="Print HTML page to stderr"
    )

    parser.add_argument(
        "filename", help="Filename to look for"
    )

    args = parser.parse_args(argv)

    if args.distro is None:
        distro = find_current_distro()
    else:
        distro = args.distro

    html = fetch_page(distro, args.filename)

    if args.debug:
        print(html, file=sys.stderr)

    packages = parse_html(html)
    table = [
        (x.name, x.arch, x.path)
        for x in sorted(packages, key=lambda p: (p.name, p.arch))
    ]
    print(tabulate(table, ["Name", "Arch", "Path"]))


if __name__ == "__main__":
    main()
